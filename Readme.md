# DComp Site Ansible Role

This role is split up into multiple parts. None of the parts run by default,
they can be selected by setting the associated trigger variable to `yes`. The
parts include.

- [keepalived](#keepalived)

## Keepalived

**trigger:** `keepalived` 

| name | required | default | description |
| ---- | -------- | ------- | ----------- |
| master | **master or backup** | | Set up a master instance |
| backup | **master or backup** | | Set up a backup instance |
| ext\_interface | **yes** | | The external interface to manage |
| ext\_interface | **yes** | | The internal interface to manage |
| ext\_peer | **yes** | | External primary peer address |
| ext\_src | **yes** | | External primary source address |
| ext\_routerid | **yes**| | The id of the external virtual router |
| int\_routerid | **yes**| | The id of the internal virtual router |
| ext\_virtual\_address | **yes** | | The desired external virtual floating IP |
| int\_virtual\_address | **yes** | | The desired internal virtual floating IP |
| vrrp\_password | **yes** | | Password to use for the vrrp protocol |

### Example

```yaml
import_role:
  name: site
vars:
  keepalived: yes
  master: yes
  ext_interface: eth0
  int_interface: eth1
  ext_peer: 1.2.3.50
  ext_src: 1.2.3.51
  ext_routerid: 10
  int_routerid: 20
  ext_virtual_address: 1.2.3.4
  int_virtual_address: 10.0.0.99
  vrrp_password: muffins
```

